grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat)+ EOF!;

stat
    : expr NL -> expr

    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | PRINT expr NL -> ^(PRINT expr)
    | ifExpr NL -> ifExpr
    | doExpr NL -> doExpr
    | whileExpr NL -> whileExpr
    | NL ->
    ;

ifExpr
	  : IF^ LP! condition RP! THEN! expr (ELSE! expr)?
	  ;

doExpr
    : DO^ condition WHILE! LP! condition RP!
    ;

whileExpr
    : WHILE^ LP! condition RP! THEN! expr
    ;

condition
    : expr
      ( EQ^ expr
      | NE^ expr
    )*
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : powExpr
      ( MUL^ powExpr
      | DIV^ powExpr
      )*
    ;

powExpr
    : atom 
      (POW^ atom
      |SQRT^
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

PRINT : 'print';

VAR :'var';

IF
  : 'if'
  ;

DO
  : 'do'
  ;

WHILE
  : 'while'
  ;

THEN
  : 'then'
  ;

ELSE
  : 'else'
  ;

EQ
  : '=='
  ;

NE
  : '!='
  ;

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	
POW
  : '^'
  ;
  
SQRT
  : '#'
  ;  