tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer iterator = 0;
}
prog    : (e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d});

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> substract(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> multiply(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> divide(p1={$e1.st},p2={$e2.st})
        | INT                      -> int(i={$INT.text})
        | ^(EQ    e1=expr e2=expr) -> equal(p1={$e1.st},p2={$e2.st})
        | ^(NE    e1=expr e2=expr) -> notequal(p1={$e1.st},p2={$e2.st})
        | ^(IF    e1=expr e2=expr e3=expr?) {iterator++;} -> if(c={$e1.st},p1={$e2.st},p2={$e3.st}, it={iterator.toString()})
        | ^(DO    e1=expr e2=expr) {iterator++;} -> do(p1={$e1.st},c={$e2.st}, it={iterator.toString()})
        | ^(WHILE e1=expr e2=expr) {iterator++;} -> while(p1={$e1.st},c={$e2.st}, it={iterator.toString()})
        | ^(PODST i1=ID   e2=expr)
    ;
    