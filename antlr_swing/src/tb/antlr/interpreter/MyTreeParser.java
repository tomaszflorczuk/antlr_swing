package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

public class MyTreeParser extends TreeParser {

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer add(Integer e1, Integer e2) {
		return e1 + e2;
	}

	protected Integer substract(Integer e1, Integer e2) {
		return e1 - e2;
	}

	protected Integer multiply(Integer e1, Integer e2) {
		return e1 * e2;
	}

	protected Integer divide(Integer e1, Integer e2) throws Exception {
		if(e2 == 0)	throw new Exception("Błąd, dzielenie przez 0 niedozwolone!");
		return e1 / e2;
	}

	protected Integer power(Integer e1, Integer e2) {
		return (int) Math.pow(e1, e2);
	}
	
	protected Integer sqrt(Integer e1) {
		return (int) Math.sqrt(e1);
	}
}
